from waitress import serve
from flaskpage.config import Config
from flaskpage import create_app

app = create_app(Config)

serve(app, host='192.168.0.100', port=5500)
#serve(app, host=Config.SERVER_NAME.split(":")[0], port=Config.SERVER_NAME.split(":")[1])