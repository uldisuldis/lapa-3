from flaskpage import db, create_app
from flaskpage.models import User, Homework, DoneHomework, Stats
from flaskpage.config import ConfigLH
create_app(ConfigLH).app_context().push()
