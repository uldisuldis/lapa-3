import os
import secrets
from PIL import Image
from flask import url_for, current_app
from flask_mail import Message
from flaskpage import mail


def save_picture(form_picture):
    random_hex = secrets.token_hex(8)
    _, f_ext = os.path.splitext(form_picture.filename)
    picture_fn = random_hex + f_ext
    picture_path = os.path.join(current_app.root_path, 'static/profile_pics', picture_fn)

    output_size = (125, 125)
    i = Image.open(form_picture)
    i.thumbnail(output_size)
    i.save(picture_path)

    return picture_fn

def send_reset_email(user):
    token = user.get_reset_token()
    msg = Message('Password Reset Request',
                  sender='noreply@va.lv',
                  recipients=[user.email])
    msg.body = f'''A request has bin sent to reset your ViA Helper password.
To reset your password visit the following link:
{url_for('users.reset_token', token=token, _external=True)}
If you did not make this request. Ignore this email and no changes will be made.
'''
    mail.send(msg)

def send_remainder_email(email, title, body):
    msg = Message('Homework Remainder',
                  sender='noreply@va.lv',
                  recipients=[email])
    msg.body = f'''You have bin sent a homework remainter.
The following paragraphs contain information about the homework.

{title}

{body}

You can also view the homework on the ViA Helper website, by cliking the link below.
{url_for('main.homework', _external=True)}
'''
    mail.send(msg)
