from datetime import datetime
from flask import render_template, url_for, flash, abort, redirect, request, Blueprint
from flask_login import login_user, current_user, logout_user, login_required
from flaskpage import db, bcrypt
from flaskpage.models import User, Homework, DoneHomework, Stats
from flaskpage.utils import save_picture, send_reset_email, send_remainder_email
from flaskpage.forms import (PostForm, RegistrationForm, LoginForm,
							UpdateAccountForm, RequestResetForm, ResetPasswordForm)

postsBP = Blueprint('posts', __name__)
usersBP = Blueprint('users', __name__)
mainBP = Blueprint('main', __name__)
errorsBP = Blueprint('errors', __name__)


@mainBP.route("/about")
def about():
	return render_template('about.html', title='About')

@mainBP.route("/stats")
def stats():
	userNum = User.query.count()
	adminNum = User.query.filter_by(op=True).count()
	postNum = Homework.query.count()
	donehwNum = Stats.query.get(0).comp_hw
	yearhwNum = DoneHomework.query.count()
	remeNum = Stats.query.get(0).sent_rem
	actiUse = User.query.filter_by(op=False).all()
	actiNum = 0
	for Use in actiUse:
		if (datetime.utcnow()-Use.last_seen).days <= 5:
			actiNum += 1
	return render_template('stats.html', title='Statistics', userNum=userNum, remeNum=remeNum, yearhwNum=yearhwNum,
					adminNum=adminNum, postNum=postNum, donehwNum=donehwNum, actiNum=actiNum)

@usersBP.route("/users", methods=['GET', 'POST'])
def users():
	if not current_user.op:
		return redirect(url_for('main.about'))
	else:
		form = RegistrationForm()
		page = request.args.get('page', 1, type=int)
		users = User.query.order_by(User.username.desc()).paginate(page=page, per_page=20)
		if form.validate_on_submit():
			hashed_password = bcrypt.generate_password_hash(form.password.data).decode('utf-8')
			user = User(username=form.username.data, email=form.email.data, subj=form.subj.data, year=form.year.data, op=form.op.data, password=hashed_password)
			db.session.add(user)
			db.session.commit()
			flash('Account has been created successfully!', 'success')
			return redirect(url_for('users.users'))
		return render_template('users.html', AllUsers=users, title='Users', form=form)

@usersBP.route("/account/<int:user_id>", methods=['GET', 'POST'])
@login_required
def account(user_id):
	if not current_user.op and current_user.id != user_id:
		return redirect(url_for('main.about'))
	else:
		user = User.query.filter_by(id=user_id).first_or_404()
		form = UpdateAccountForm()
		form.user_id = user_id
		if form.validate_on_submit():
			if user.subj != form.subj.data or user.year != form.year.data:
				done_hw = DoneHomework.query.filter_by(user_id=user_id).first()
				while done_hw:
					db.session.delete(done_hw)
					done_hw = DoneHomework.query.filter_by(user_id=user_id).first()
			if form.picture.data:
				picture_file = save_picture(form.picture.data)
				user.image_file = picture_file
			user.username = form.username.data
			user.email = form.email.data
			user.subj = form.subj.data
			user.year = form.year.data
			user.op = form.op.data
			db.session.commit()
			flash('Account has been updated!', 'success')
			return redirect(url_for('users.account', user_id=user.id))
		elif request.method == 'GET':
			form.username.data = user.username
			form.email.data = user.email
			form.subj.data = user.subj
			form.year.data = user.year
			form.op.data = user.op
		image_file = url_for('static', filename='profile_pics/' + user.image_file)
		page = request.args.get('page', 1, type=int)
		comp_hw = DoneHomework.query
		user_hw = User.query
		if user.op:
			posts = Homework.query.filter_by(user_id=user.id)\
							.order_by(Homework.date_posted.desc())\
							.paginate(page=page, per_page=20)
		else:
			posts = Homework.query.filter_by(subj=user.subj)\
							.filter_by(year=user.year)\
							.order_by(Homework.date_posted.desc())\
							.paginate(page=page, per_page=20)
		return render_template('account.html', title='Account', posts=posts, comp_hw=comp_hw,
								user=user, user_hw=user_hw, image_file=image_file, form=form)

@usersBP.route("/user/<int:user_id>/delete", methods=['GET', 'POST'])
@login_required
def delete_user(user_id):
	user = User.query.filter_by(id=user_id).first_or_404()
	if not current_user.op and current_user.id != user_id:
		abort(403)
	post = Homework.query.filter_by(user_id=user_id).first()
	while post:
		done_hw = DoneHomework.query.filter_by(homework_id=post.id).first()
		while done_hw:
			db.session.delete(done_hw)
			done_hw = DoneHomework.query.filter_by(homework_id=post.id).first()
		db.session.delete(post)
		post = Homework.query.filter_by(user_id=user_id).first()
	db.session.delete(user)
	db.session.commit()
	flash('User has been deleted!', 'success')
	return redirect(url_for('users.users'))

@mainBP.route("/")
@mainBP.route("/homework", methods=['GET', 'POST'])
@login_required
def homework():
	form = PostForm()
	if form.validate_on_submit():
		post = Homework(user_id=current_user.id, title=form.title.data, subj=form.subj.data, year=form.year.data, content=form.content.data)
		db.session.add(post)
		db.session.commit()
		flash('Your post has been created!', 'success')
		return redirect(url_for('main.homework'))
	page = request.args.get('page', 1, type=int)
	if current_user.op:
		posts = Homework.query.filter_by(user_id=current_user.id)\
						.order_by(Homework.date_posted.desc())\
						.paginate(page=page, per_page=20)
	else:
		posts = Homework.query.filter_by(subj=current_user.subj)\
						.filter_by(year=current_user.year)\
						.order_by(Homework.date_posted.desc())\
						.paginate(page=page, per_page=20)
	comp_hw = DoneHomework.query
	user_hw = User.query
	return render_template('homework.html', posts=posts, user_hw=user_hw, title='Homework',
						   form=form, comp_hw=comp_hw, legend='Add Homework')

@postsBP.route("/post/<int:post_id>")
def post(post_id):
	if not current_user.op:
		abort(403)
	post = Homework.query.get_or_404(post_id)
	Users = User.query.filter_by(subj=post.subj, year=post.year, op=False).all()
	done_hw = DoneHomework.query.filter_by(homework_id=post.id)
	data_count = DoneHomework.query.filter_by(homework_id=post.id).count()
	data_total = User.query.filter_by(subj=post.subj, year=post.year).count()
	return render_template('post.html', title='Homework', Users=Users, done_hw=done_hw,
							data_count=data_count, data_total=data_total, post=post)

@postsBP.route("/post/<int:post_id>/<int:user_id>")
def post_done(post_id, user_id):
	post = Homework.query.get_or_404(post_id)
	if not current_user.op or post.author.id != current_user.id:
		abort(403)
	done_hv = DoneHomework.query.filter_by(user_id=user_id, homework_id=post_id).first()
	if done_hv:
		db.session.delete(done_hv)
		Stats.query.get(0).comp_hw -= 1
		db.session.commit()
	else:
		done_hv = DoneHomework(user_id=user_id, homework_id=post_id)
		db.session.add(done_hv)
		Stats.query.get(0).comp_hw += 1
		db.session.commit()
	return redirect(url_for('posts.post', post_id=post_id))

@postsBP.route("/post/<int:post_id>/remainders", methods=['GET', 'POST'])
@login_required
def send_remainders(post_id):
	post = Homework.query.get_or_404(post_id)
	if not current_user.op or post.author.id != current_user.id:
		abort(403)
	users = User.query.filter_by(subj=post.subj, year=post.year, op=False).all()
	for user in users:
		send_remainder_email(user.email, post.title, post.content)
		Stats.query.get(0).sent_rem += 1
		db.session.commit()
	flash(f'Remainders for post {post.title} has been sent!', 'success')
	return redirect(url_for('main.homework'))

@postsBP.route("/post/<int:post_id>/update", methods=['GET', 'POST'])
@login_required
def update_post(post_id):
	post = Homework.query.get_or_404(post_id)
	if not current_user.op or post.author.id != current_user.id:
		abort(403)
	form = PostForm()
	if form.validate_on_submit():
		if post.subj != form.subj.data or post.year != form.year.data:
			done_hw = DoneHomework.query.filter_by(homework_id=post_id).first()
			while done_hw:
				db.session.delete(done_hw)
				done_hw = DoneHomework.query.filter_by(homework_id=post_id).first()
		post.title = form.title.data
		post.subj = form.subj.data
		post.year = form.year.data
		post.content = form.content.data
		db.session.commit()
		flash('Your post has been updated!', 'success')
		return redirect(url_for('posts.post', post_id=post.id))
	elif request.method == 'GET':
		form.title.data = post.title
		form.subj.data = post.subj
		form.year.data = post.year
		form.content.data = post.content
	return render_template('edit_post.html', title='Homework',
						   form=form, legend='Update Post')

@postsBP.route("/post/<int:post_id>/delete", methods=['GET', 'POST'])
@login_required
def delete_post(post_id):
	post = Homework.query.get_or_404(post_id)
	if not current_user.op or post.author.id != current_user.id:
		abort(403)
	db.session.delete(post)
	db.session.commit()
	flash('Your post has been deleted!', 'success')
	return redirect(url_for('main.homework'))

@usersBP.route("/login", methods=['GET', 'POST'])
def login():
	if current_user.is_authenticated:
		return redirect(url_for('main.homework'))
	form = LoginForm()
	if form.validate_on_submit():
		user = User.query.filter_by(email=form.email.data).first()
		if user and bcrypt.check_password_hash(user.password, form.password.data):
			login_user(user, remember=form.remember.data)
			next_page = request.args.get('next')
			user.last_seen = datetime.utcnow()
			db.session.commit()
			return redirect(next_page) if next_page else redirect(url_for('main.homework'))
		else:
			flash('Login Unsuccessful. Please check email and password', 'danger')
	return render_template('login.html', title='Login', form=form)

@usersBP.route("/logout")
def logout():
	logout_user()
	return redirect(url_for('main.about'))

@usersBP.route("/reset_password", methods=['GET', 'POST'])
def reset_request():
	if current_user.is_authenticated:
		return redirect(url_for('main.homework'))
	form = RequestResetForm()
	if form.validate_on_submit():
		user = User.query.filter_by(email=form.email.data).first()
		send_reset_email(user)
		flash('An email has been sent with instructions to reset your password.', 'info')
		return redirect(url_for('users.login'))
	return render_template('reset_request.html', title='Reset Password', form=form)

@usersBP.route("/reset_password/<token>", methods=['GET', 'POST'])
def reset_token(token):
	if current_user.is_authenticated:
		return redirect(url_for('main.homework'))
	user = User.verify_reset_token(token)
	if user is None:
		flash('That is an invalid or expired token', 'warning')
		return redirect(url_for('users.reset_request'))
	form = ResetPasswordForm()
	if form.validate_on_submit():
		hashed_password = bcrypt.generate_password_hash(form.password.data).decode('utf-8')
		user.password = hashed_password
		db.session.commit()
		flash('Your password has been updated! You are now able to log in', 'success')
		return redirect(url_for('users.login'))
	return render_template('reset_token.html', title='Reset Password', form=form)

@errorsBP.app_errorhandler(404)
def error_404(error):
	return render_template('errors/404.html'), 404

@errorsBP.app_errorhandler(403)
def error_403(error):
	return render_template('errors/403.html'), 403

@errorsBP.app_errorhandler(500)
def error_500(error):
	return render_template('errors/500.html'), 500
