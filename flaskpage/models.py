from datetime import datetime
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer
from flask import current_app
from flaskpage import db, login_manager
from flask_login import UserMixin


@login_manager.user_loader
def load_user(user_id):
	return User.query.get(int(user_id))

class User(db.Model, UserMixin):
	id = db.Column(db.Integer, primary_key=True)
	username = db.Column(db.String(100), unique=True, nullable=False)
	email = db.Column(db.String(100), unique=True, nullable=False)
	password = db.Column(db.String(100), nullable=False)
	subj = db.Column(db.String(100), nullable=False)
	year = db.Column(db.String(100), nullable=False)
	op = db.Column(db.Boolean, nullable=False, default=False)
	image_file = db.Column(db.String(20), nullable=False, default='default.png')
	posts = db.relationship('Homework', backref='author', lazy=True)
	last_seen = db.Column(db.DateTime, nullable=False, default=datetime(1, 1, 1, 0, 0))

	def get_reset_token(self, expires_sec=1800):
		s = Serializer(current_app.config['SECRET_KEY'], expires_sec)
		return s.dumps({'user_id': self.id}).decode('utf-8')

	@staticmethod
	def verify_reset_token(token):
		s = Serializer(current_app.config['SECRET_KEY'])
		try:
			user_id = s.loads(token)['user_id']
		except:
			return None
		return User.query.get(user_id)

	def __repr__(self):
		return f"User('{self.id}', '{self.username}', '{self.email}', '{self.subj}', '{self.year}', '{self.op}', '{self.last_seen}', '{self.image_file}')"

class Homework(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
	title = db.Column(db.String(100), nullable=False)
	subj = db.Column(db.String(100), nullable=False)
	year = db.Column(db.String(100), nullable=False)
	content = db.Column(db.Text, nullable=False)
	date_posted = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)

	def __repr__(self):
		return f"Homework('{self.id}', '{self.user_id}', '{self.title}', '{self.subj}', '{self.year}', '{self.date_posted}')"

class DoneHomework(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
	homework_id = db.Column(db.Integer, db.ForeignKey('homework.id'), nullable=False)

	def __repr__(self):
		return f"DoneHomework('{self.id}', '{self.user_id}', '{self.post_id}')"

class Stats(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	comp_hw = db.Column(db.Integer, nullable=False, default=0)
	sent_rem = db.Column(db.Integer, nullable=False, default=0)

	def __repr__(self):
		return f"Stats('{self.comp_hw}', '{self.sent_rem}')"