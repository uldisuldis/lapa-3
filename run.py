from flaskpage.config import ConfigLH
from flaskpage import create_app

app = create_app(ConfigLH)

if __name__ == '__main__':
    app.run(debug=True)
